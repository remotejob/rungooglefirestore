export GOOGLE_APPLICATION_CREDENTIALS=activedomains-db3b02b21cff.json


go mod init gitlab.com/remotejob/googledatastore
go get cloud.google.com/go/firestore
go get google.golang.org/api/iterator


gcloud builds submit --tag gcr.io/activedomains/googledatastore


gcloud beta run deploy --image gcr.io/activedomains/googledatastore --platform managed

go run gitlab.com/remotejob/googledatastore/cmd/uploaddata


go build -o bin/uploaddata gitlab.com/remotejob/rungooglefirestore/cmd/uploaddata

